export class MetadataInfoBuilder {
  metadata = {
    media: [],
    data: {},
  };

  constructor() {}

  /**
   *
   * @param {string} type
   * @param {string} link
   * @returns current instance of the class
   */
  addMedia(type, link) {
    this.metadata.media.push({ type, link });
    return this;
  }

  /**
   *
   * @param {object} data
   * @returns current instance of the class
   */
  addData(data) {
    this.metadata.data = { ...this.metadata.data, ...data };
    return this;
  }

  /**
   *
   * @returns JSON of metadata
   */
  getData() {
    return JSON.stringify(this.data);
  }
}
