export class MetadataInfoBuilder {
  reward = {
    
  };

  constructor() {}

  /**
   *
   * @param {string} type
   * @param {string} link
   * @returns current instance of the class
   */
  addRewardType(type, link) {
    this.metadata.media.push({ type, link });
    return this;
  }

  /**
   *
   * @param {object} data
   * @returns current instance of the class
   */
  addData(data) {
    this.metadata.data = { ...this.metadata.data, ...data };
    return this;
  }

  /**
   *
   * @returns JSON of metadata
   */
  getData() {
    return JSON.stringify(this.data);
  }
}
