import { create } from "ipfs-http-client";
const client = create("http://ec2-3-238-189-66.compute-1.amazonaws.com:5001");

export class IpfsGeerService {
  async sendJSONToIpfs(data) {
    return await client.add(JSON.stringify(data));
  }
}

// const test = new IpfsGeerService();

// test.sendJSONToIpfs({ test: "w" }).then((result) => {
//   console.log(result);
// });
