const { contract } = require("@openzeppelin/test-environment");

const { expect } = require("chai");

const GeerProducts = contract.fromArtifact("GeerProducts");

const createTestProduct = async (contract) => {
  await contract.registerProductType(
    "1",
    "Product name",
    "Product description",
    "Nike",
    "http://localhost",
    "http://localhost"
  );
};

describe("GeerProducts", function () {
  beforeEach(async function () {
    this.myContract = await GeerProducts.new();
  });

  it("The products array is empty initially", async function () {
    expect((await this.myContract.getProductsSize()).toString()).to.equal("0");
  });

  it("The products array size is 1 after adding a product", async function () {
    await createTestProduct(this.myContract);

    expect((await this.myContract.getProductsSize()).toString()).to.equal("1");
  });

  it("The same product is returned after adding a product", async function () {
    const expectedName = "Product name";
    await createTestProduct(this.myContract);
    const product = await this.myContract.getProductType("1");
    console.log("product", product);

    expect(product[1]).to.equal(expectedName);
  });

  it("Mint a token for the particular product", async function () {
    await createTestProduct(this.myContract);
    await this.myContract.mint("1", 1, "Coupon code", true);
    const productDetails = await this.myContract.getItemDetails("1");
    console.log(productDetails);

    expect(productDetails[0][0]).to.equal("1");
  });

  it("Mint a token for the particular product", async function () {
    await createTestProduct(this.myContract);
    await this.myContract.mint("1", 1, "Coupon code", true);
    await this.myContract.setIsUnlockedRewardForItem(1, false);
    const productDetails = await this.myContract.getItemDetails(1);

    expect(productDetails[0][3]).to.equal(false);
  });
});
