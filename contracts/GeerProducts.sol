// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

/**
    The struct describes a product model
    Product model is described in this document
    https://docs.google.com/document/d/1X7zGgsTgTLsbYk4EQ_WwPY3R7cXJhNns_-dXLs45WXw/edit#heading=h.64msh65jiiug
 */
struct Product {
    string id;
    string name;
    string description;
    string author;
    string metadataURI;
    string rewardURI;
}

/**
    TokenData is a description model for a particular Item of Product
 */
struct TokenData {
    string productId;
    uint256 rank;
    string couponCode;
    bool isUnlocked;
}

/**
    The struct is used to return combined data of Product and Item
 */
struct ItemDetails {
    TokenData token;
    Product product;
}

/// @title Geer products ownershop via NFT.
contract GeerProducts is ERC721, ERC721Pausable, Ownable {
    mapping(string => Product) public products;
    mapping(uint256 => TokenData) internal tokenData;
    string[] public productKeys;

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    Counters.Counter private _productIds;

    event ProductRegistered(address _author, string indexed _productId);

    constructor() ERC721("Geer Products", "GEER") {}

    /**
        mint method
        argument productId {string} - product relation
        argument rank {uint256} - rank or particular item
        argument couponCode {string} - encrypted coupon code
        argument isUnlocked {bool} - determines if a reward of the item is unlocked
     */
    function mint(
        string memory productId,
        uint256 rank,
        string memory couponCode,
        bool isUnlocked
    ) public onlyOwner returns (uint256) {
        require(bytes(products[productId].id).length > 0, "Given product ID does not exist");
        _tokenIds.increment();

        uint256 ID = _tokenIds.current();
        _safeMint(msg.sender, ID);
        tokenData[ID] = TokenData(productId, rank, couponCode, isUnlocked);

        return ID;
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override(ERC721, ERC721Pausable) {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal virtual override(ERC721) {
        super._burn(tokenId);
    }

    function setIsUnlockedRewardForItem(uint256 tokenDataId, bool isUnlockedStatus) public {
        tokenData[tokenDataId].isUnlocked = isUnlockedStatus;
    }

    /**
        registerProductType method adds another Product to blockchain. All the items will have a realtion to a product
     */
    function registerProductType(
        string memory id,
        string memory name,
        string memory description,
        string memory author,
        string memory metadataURI,
        string memory rewardURI
    ) public {
        // _productIds.increment();
        // uint256 ID = _productIds.current();
        products[id] = Product(
            id,
            name,
            description,
            author,
            metadataURI,
            rewardURI
        );
        productKeys.push(id);
        emit ProductRegistered(msg.sender, id);
    }

    function getProductType(string memory productId)
        public
        view
        returns (Product memory)
    {
        Product memory product = products[productId];
        require(bytes(products[productId].id).length > 0, "Given product ID does not exist");
        return product;
    }

    function productsAmount() public view returns (uint256) {
        return uint256(productKeys.length);
    }

    function getItemDetails(uint256 tokenId)
        public
        view
        returns (ItemDetails memory)
    {
        TokenData memory token = tokenData[tokenId];
        Product memory product = products[token.productId];
        return ItemDetails(token, product);
    }

    function getProductsSize() public view returns (uint256) {
        return productKeys.length;
    }
}
